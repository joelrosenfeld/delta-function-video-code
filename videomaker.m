%% For the Delta Function Video


%% Melting Funciton

v = VideoWriter('meltingfunction.avi');
open(v);
figure('Renderer', 'painters', 'Position', [10 10 900 600])

t = -3:0.01:3;

f = @(x) sin(4*x)+x-5*exp(-(x-2).^2)+1;

for i = 0:300
    
gaussianfn = @(x) 1/sqrt(2*pi)*exp(i/100)*exp(-1/2*(1/exp(-i/100)^2*x.^2));
y = f(t).*gaussianfn(t);
pause(0.01);
plot(t,y,'LineWidth',3);

title("Function times Gaussian");

xlabel("x");
ylabel("y");

axis([-3 3 1.2*min(y) 1.1*max(y)]);

writeVideo(v,getframe(gcf));

end

for j = 1:3:length(t)
    
gaussianfn = @(x) 1/sqrt(2*pi)*exp(i/100)*exp(-1/2*(1/exp(-i/100)^2*x.^2));
y = f(t).*gaussianfn(t);
pause(0.01);
plot(t,y,'LineWidth',3);
hold on
area(t(1:j),y(1:j));
hold off

axis([-3 3 1.2*min(y) 1.1*max(y)]);

title("Function times Gaussian");

xlabel("x");
ylabel("y");

writeVideo(v,getframe(gcf));
end

close(v);

%% Draw Area Beginning
v = VideoWriter('meltingfunction_beginintegration.avi');
open(v);
figure('Renderer', 'painters', 'Position', [10 10 900 600])
i=0;

for j = 1:3:length(t)
    
gaussianfn = @(x) 1/sqrt(2*pi)*exp(i/100)*exp(-1/2*(1/exp(-i/100)^2*x.^2));
y = f(t).*gaussianfn(t);
pause(0.01);
plot(t,y,'LineWidth',3);
hold on
area(t(1:j),y(1:j));
hold off

axis([-3 3 1.2*min(y) 1.1*max(y)]);

title("Function times Gaussian");

xlabel("x");
ylabel("y");

writeVideo(v,getframe(gcf));
end

close(v);

%% Draw Funciton

v = VideoWriter('originalfunction.avi');
open(v);
figure('Renderer', 'painters', 'Position', [10 10 900 600])

t = -3:0.01:3;

f = @(x) sin(4*x)+x-5*exp(-(x-2).^2)+1;

for i = 1:length(t)
    
gaussianfn = @(x) 1/sqrt(2*pi)*exp(i/100)*exp(-1/2*(1/exp(-i/100)^2*x.^2));
y = f(t(1:i));
pause(0.01);
plot(t(1:i),y,'LineWidth',3);

title("Original Function");

xlabel("x");
ylabel("y");

axis([-3 3 1.2*(-1.994825723211434) 1.1*2.016293974324401]);

writeVideo(v,getframe(gcf));

end

close(v);


%% Draw Gaussian

v = VideoWriter('gaussiansquish.avi');
open(v);
figure('Renderer', 'painters', 'Position', [10 10 900 600])

t = -3:0.01:3;

f = @(x) sin(4*x)+x-5*exp(-(x-2).^2)+1;

for i = 0:300
    
gaussianfn = @(x) 1/sqrt(2*pi)*exp(i/100)*exp(-1/2*(1/exp(-i/100)^2*x.^2));
y = gaussianfn(t);
pause(0.01);
plot(t,y,'LineWidth',3);

title("Gaussian Functions");

xlabel('x');
ylabel('y');

axis([-3 3 0 1.1*max(y)]);

writeVideo(v,getframe(gcf));

end

close(v);

%% Draw Sinc

v = VideoWriter('sincfunction.avi');
open(v);
figure('Renderer', 'painters', 'Position', [10 10 900 600])

t = -20:0.1:20;
    
sincfn = @(x) sin(x)./x;
y = sincfn(t);

for j = 1:length(y)
    if(isnan(y(j)) || isinf(y(j)))
        y(j) = 1;
    end
end

for i = 1:length(y)


pause(0.01);
plot(t(1:i),y(1:i),'LineWidth',3);

title("Sinc Function");

xlabel('x');
ylabel('y');

axis([-20 20 1.1*min(y) 1.1*max(y)]);

writeVideo(v,getframe(gcf));

end

close(v);


%% Draw Compactly Supported Function

v = VideoWriter('functiontransform.avi');
open(v);
figure('Renderer', 'painters', 'Position', [10 10 1800 300])

t = -9:0.05:9;

f = @(x) (abs(x) < 3).*exp(-1./(9-x.^2)).*(sin(4*x)+x-5*exp(-(x-2).^2)+1);


for i = 1:length(t)
    
%gaussianfn = @(x) 1/sqrt(2*pi)*exp(i/100)*exp(-1/2*(1/exp(-i/100)^2*x.^2));
y = f(t(1:i));


plot(t(1:i),y,'LineWidth',3);

title("Compactly Supported Smooth Function");

xlabel('x');
ylabel('y');

axis([-9 9 1.1*(-1.6287) 1.1*1.801]);

writeVideo(v,getframe(gcf));

end

% Pause 3 Seconds
for i = 1:90
    writeVideo(v,getframe(gcf));
end

syms x1;

f1 = exp(-1/10*x1^2)*(sin(4*x1)+x1-5*exp(-(x1-2)^2)+1);

diff_f1 = diff(f1);

diff_f = matlabFunction(diff_f1);

diff_y = diff_f(t);

diff_y2 = diff_y.*t.^2;

f2 = @(x) exp(-1/10*x.^2).*(sin(4*x)+x-5*exp(-(x-2).^2)+1);
y2 = f2(t);
y = f(t);
for i = 1:101
   
transitiony = (1-(i-1)/100)*y + (i-1)/100*y2;

plot(t,transitiony,'LineWidth',3);

title("Rapidly Decreasing Function");

xlabel('x');
ylabel('y');

axis([-9 9 1.1*min(transitiony) 1.1*max(transitiony)]);

writeVideo(v,getframe(gcf));

end

% Pause 3 Seconds
for i = 1:90
    writeVideo(v,getframe(gcf));
end

for i = 1:601
   
%transitiony = (1-(i-1)/100)*y2 + (i-1)/100*diff_y;
trans = @(x) (x < 1/2).*(x > 0).*1/2.*(cos(2*pi*(x))+1) + (x <= 0).*1 + (x >= 1/2).*0;
transitiony = (1-trans(floor(t/1)/10+10-20*(i/601))).*y2 + trans(floor(t/1)/10+10-20*(i/601)).*diff_y;

plot(t,transitiony,'LineWidth',3);

title("Rapidly Decreasing Function Derivative");

xlabel('x');
ylabel('y');

axis([-9 9 1.1*min(transitiony) 1.1*max(transitiony)]);

writeVideo(v,getframe(gcf));
end

% Pause 3 Seconds
for i = 1:90
    writeVideo(v,getframe(gcf));
end

for i = 1:101
   
transitiony = (1-(i-1)/100)*diff_y + (i-1)/100*y2;
plot(t,transitiony,'LineWidth',3);

title("Rapidly Decreasing Function Derivative Times x^2");

xlabel('x');
ylabel('y');

axis([-9 9 1.1*min(transitiony) 1.1*max(transitiony)]);

writeVideo(v,getframe(gcf));

end

for i = 1:90
    writeVideo(v,getframe(gcf));
end

close(v);