# README #

This will use MATLAB's VideoWriter function to generate a sequence of AVI files that correspond to the figures in the YouTube video:

What is a delta function? From distributions to reproducing kernels.
https://youtu.be/kA3r4Td2E3E

If you use this for your own work, please point your viewers to http://www.youtube.com/c/ThatMathThing